<?php

/***********************************
 * HOOKS
 **********************************/
/**
 * Implementation of hook_menu()
 */
function salesforce_query_select_menu()
{
  $items['admin/structure/salesforce_query/select'] = array(
    'title' => 'SELECT Queries',
    'page callback' => 'salesforce_query_list',
    'page arguments' => array('select', array('condition')),
    'access arguments' => array('salesforce_query_call'),
  );

  $items['admin/structure/salesforce_query/select/list'] = array(
    'title' => 'List',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -10,
  );

  $items['admin/structure/salesforce_query/select/create'] = array(
    'title' => 'Create',
    'type' => MENU_LOCAL_TASK,
    'page callback' => 'drupal_get_form',
    'page arguments' => array('salesforce_query_select_create_form'),
    'access arguments' => array('salesforce_query_create'),
  );

  $items['admin/structure/salesforce_query/select/edit/%'] = array(
    'title' => 'Edit',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('salesforce_query_select_create_form', 5),
    'access arguments' => array('salesforce_query_create'),
  );

  $items['salesforce_query/select/delete/%'] = array(
    'page callback' => 'salesforce_query_delete',
    'page arguments' => array(1, 3),
    'access arguments' => array('salesforce_query_delete'),
  );

  return $items;
}

/**************************************
 * PAGE CALLBACKS
 *************************************/
function salesforce_query_select_call_service($name, $conditions = NULL)
{
  $service_data = variable_get("salesforce_query_select_$name");

  $query = new SalesforceSelectQuery($service_data['sobject']);

  if($conditions)
  {
    $conditions = _salesforce_query_validate_select_textfield_parameters($conditions, "condition", $service_data['condition']);
  }
  else
  {
    $postbody = @file_get_contents('php://input');
    $post_array = drupal_json_decode($postbody);
    $conditions = _salesforce_query_validate_select_textfield_parameters($post_array, "condition", $service_data['condition']);
  }

  $operators = variable_get('salesforce_query_operators', array("=", "!=", "<", ">", "LIKE", "IN", "NOT IN"));
  $force = salesforce_get_api();
  $object = $force->objectDescribe($service_data['sobject']);

  foreach($conditions as $key => $value)
  {
    foreach($object['fields'] as $field)
    {
      if($field['name'] == $key)
      {
        if($field['type'] == 'boolean')
        {
          $conditions[$key] = filter_var($value, FILTER_VALIDATE_BOOLEAN);
        }
      }
    }
  }

  foreach($conditions as $key => $value)
  {
    $operator = $operators[$service_data['operators']['condition'][$key]];
    if(is_bool($value))
    {
      $text = 'FALSE';
      if($value) $text = 'TRUE';
      $query->addCondition($key, $text, $operator);
    }
    else
    {
      $query->addCondition($key, "'" . urlencode(check_url($value)) . "'", $operator);
    }
  }

  foreach($service_data['field'] as $key => $value)
  {
    $query->fields[] = $value;
  }

  //If we reach this point, we assume that we are good to call
  $force = salesforce_get_api();
  $result = $force->query($query);
  return drupal_json_encode($result['records']);
}

function salesforce_query_select_create_form($form, &$form_state, $service_id = null)
{
  $textfield_selects = array();
  $textfield_selects[] = array('singular' => 'condition', 'plural' => 'conditions');

  $selects = array();
  $selects[] = array('singular' => 'field', 'plural' => 'fields');

  $form = _salesforce_query_create_form('select', $form, $form_state, $service_id, null, $textfield_selects, $selects);
  return $form;
}

/*************************************
 * SUBMIT / VALIDATION HANDLERS
 ************************************/
function salesforce_query_select_create_form_select_validate($element, $form_state)
{
  if($element['#value'] == '0')
  {
    if($form_state['triggering_element']['#value'] == 'Add More Conditions' && strpos($element['#name'], "condition") !== FALSE)
    {
      form_error($element, t('You must enter a valid value for the ' . $element['#title'] . ' condition'));
    }
    else if($form_state['triggering_element']['#value'] == 'Add More Fields' && strpos($element['#name'], "field") !== FALSE)
    {
      form_error($element, t('You must enter a valid value for the ' . $element['#title'] . ' field'));
    }
  }
}

function salesforce_query_select_validate($form, $form_state)
{
  _salesforce_query_validate_template('select', $form_state, array('condition'), array('field'));
}

function salesforce_query_select_submit($form, $form_state)
{
  _salesforce_query_submit_template('select', $form_state, array('condition'), array('field'));
}


/***********************************
 * AJAX FUNCTIONS
 **********************************/
function _salesforce_query_select_create_sobject_ajax($form, &$form_state)
{
  return $form;
}

function _salesforce_query_select_create_add_condition_ajax($form, &$form_state)
{
  return $form['conditions'];
}

function _salesforce_query_select_create_add_fields_ajax($form, &$form_state)
{
  return $form['fields'];
}
